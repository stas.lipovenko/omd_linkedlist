
"""
>>> from list_new import List

Performing tests provided by client
>>> list_ = List(1, 2, 3)
>>> list_._print()
1 2 3 

>>> list_.append(4)
>>> list_._print()
1 2 3 4 

>>> tail = List(5, 6)
>>> list_ += tail
>>> list_._print()
1 2 3 4 5 6 

>>> tail._value = 0
>>> tail._print()
0 6 

>>> list_._print()
1 2 3 4 5 6 

>>> list_ += [7, 8]
>>> list_._print()
1 2 3 4 5 6 7 8 

>>> list_ += ()
>>> list_._print()
1 2 3 4 5 6 7 8 

# >>> for elem in list_: print(2**elem, end=' ')
# >>>     print(2**elem, end=' ')
# 2 4 8 16 32 64 128 256 

>>> list_._print_reversed()
8 7 6 5 4 3 2 1 
>>> empty_list = List()
>>> empty_list._print()
<BLANKLINE>

>>> list_with_single_none_element = List(None)
>>> list_with_single_none_element._print()
None
"""

if __name__ == "__main__":
    import doctest
    doctest.testmod()
from copy import copy

class Element:
    def __init__(self, value, next=None):
        self._value = value
        self._next = next

    """
    Set of getters/setters below
    """
    @property
    def value(self):
        """The value property."""
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    @property
    def next(self):
        """The next property."""
        return self._next

    @next.setter
    def next(self, next):
        self._next = next

    
    def __str__(self):
        """
        presents string representation for object
        """        
        return str(self.value)


class List:
    def __init__(self, *args):
        
        self.__head = None
        self.__tail = None # in order to make yeasy appending
        self.__cursor = None # in order to iterate through list easily. See __next__

        prev = None
        el = None
        # iterate through args and put them into list
        for value in args:
            el = Element(value.value if isinstance(value, Element) else value)

            if not self.__head:
                self.__head = el

            if prev:
                prev.next = el

            prev = el

        self.__cursor = self.__head
        self.__tail = el

    @property
    def _value(self):
        """Getting value of the head."""
        return self.__head.value
    @_value.setter
    def _value(self, value):
        """ Setting value of the head """        
        self.__head.value = value

    def __iter__(self):
        """Makes an iterable class"""
        return self

    def __next__(self):
        """Makes an iterator class"""  
        if self.__cursor:
            next_el, self.__cursor = self.__cursor, self.__cursor.next
            return next_el.value
        else:
            self.__cursor = self.__head
            raise StopIteration

    def __str__(self):
        """To make string print of the list"""
        result = ''
        for el in self:
            result += ' ' + str(el)
        return result.strip()
    
    def append(self, other):
        """Realizes append function
        
        Arguments:
            other {[type]} -- [description]
        """
        el = Element(other)
        self.__tail.next = el
        self.__tail = el

    def __add__(self, other):
        """Reailizes adding via +
        
        Arguments:
            other {[type]} -- [description]
        """                
        return List(*(*self,*other))
    
    def reversed(self):
        """Reversing list
        
        Returns:
            [type] -- [description]
        """     
        reversed_list = copy(self)
        reversed_list.__tail = reversed_list.__head
        current = reversed_list.__head
        prev = None
        while current:
            next_el = current.next
            current.next = prev
            prev = current
            current = next_el
                    
        reversed_list.__head = prev
        reversed_list.__cursor = reversed_list.__head
        return reversed_list

    def _print(self):
        """just print wrapper"""
        for el in self:
            print(el, end=' ')
        print()
                    
    def _print_reversed(self):
        """reversed print"""
        self.reversed()._print()

from unittest import TestCase
from core.list_new import List
from collections.abc import Iterator


class TestListClass(TestCase):
    
    def test_is_iterator(self):
        self.assertTrue(issubclass(List, Iterator))
    
    def test_init_list(self):
        self.assertEqual([*List(1, 2, 3)], [1, 2, 3])

    def test_append_single_value(self):
        list_ = List(1, 2, 3)
        list_.append(4)
        self.assertEqual([*list_], [1, 2, 3, 4])

    def test_plus_another_list(self):
        list_ = List(1, 2, 3)
        list_ += List(4, 5)
        self.assertEqual([*list_], [1, 2, 3, 4, 5])

    def test_plus_python_list(self):
        list_ = List(1, 2)
        list_ += [3, 4]
        self.assertSequenceEqual([*list_], [1, 2, 3, 4])

    def test_plus_empty_tuple(self):
        list_ = List(1, 2)
        list_ += ()
        self.assertEqual([*list_], [1, 2])

    def test_reversed_list(self):
        list_ = List(1, 2, 3, 4, 5)
        print(list_)
        self.assertEqual([*list_.reversed()], [5, 4, 3, 2, 1])

    def test_empty_list(self):
        empty_list = List()
        self.assertEqual([*empty_list], [])
